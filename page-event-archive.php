<?php
/*
 Template Name: Event Archive Page
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
							<?php if( have_rows('events_archive') ): ?>
							
								<ul class="event-archive">
							
								<?php while( have_rows('events_archive') ): the_row(); 
									// vars
									$event_name = get_sub_field('event_name');
									$details = get_sub_field('details');
									$description = get_sub_field('description');
									$link = get_sub_field('link');
									$event_image = get_sub_field('image');
									
									if( !empty($event_image) ): 
										// thumbnail
										$size = 'event-image';
										$photo = $event_image['sizes'][ $size ];
										$width = $event_image['sizes'][ $size . '-width' ];
										$height = $event_image['sizes'][ $size . '-height' ];
									endif;
								?>
									<li>
									<?php if( $link ): ?>
										<a href="<?php echo $link; ?>">
									<?php endif; ?>
											<img src="<?php echo $photo; ?>" alt="Thumbnail for event" />
									<?php if( $link ): ?>
										</a>
									<?php endif; ?>	
										<div class="event-description">
										<?php if( $link ): ?>
											<a href="<?php echo $link; ?>">
										<?php endif; ?>
												<h5><?php echo $event_name; ?></h5>
										<?php if( $link ): ?>
											</a>
										<?php endif; ?>
											<span class="event-details"><?php echo $details; ?></span>
											<?php echo $description; ?>
										</div>
									</li>
							
								<?php endwhile; ?>
							
								</ul>
							
							<?php endif; ?>
						</section>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>