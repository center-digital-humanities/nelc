<?php
/*
 Template Name:  Iranian Podcast and Videos
*/
?>
<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">

					<h1><?php the_title(); ?></h1>
					
					<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$media_loop = new WP_Query( 
						array( 'post_type' => array( 'video', 'podcast' ), 'orderby' => 'date', 'order' => 'desc', 'posts_per_page' => -1
						));
					?>
					
					<?php if ( $media_loop->have_posts() ) : while ( $media_loop->have_posts() ) : $media_loop->the_post(); ?>	

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">
						<h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
						<span class="publish-date"><strong>Published:</strong> <?php echo get_the_date(); ?></span>
						<section class="entry-content cf">
							<?php the_post_thumbnail( 'content-width' ); ?>
							<?php the_excerpt(); ?>
							<a href="<?php the_permalink() ?>" class="btn">Read More</a>
						</section>
					</article>

					<?php endwhile; ?>
					
					<?php bones_page_navi(); ?>
					
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but nothing is available at this address. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a>, <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>
					<?php wp_reset_postdata(); ?>

				</div>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php wp_nav_menu(array(
								   	'container' => false,
								   	'menu' => __( 'Iranian Studies', 'bonestheme' ),
								   	'menu_class' => 'iranian-nav',
								   	'theme_location' => 'iranian-nav',
								   	'before' => '',
								   	'after' => '',
								   	'depth' => 2,
								   	'items_wrap' => '<h3>Iranian Studies</h3> <ul>%3$s</ul>'
								));
							?>
						</nav>
					</div>
				</div>
			</div>

<?php get_footer(); ?>