<?php
/*
	Basic Navigation and Hero 
*/
?>

<a href="#main-content" class="hidden skip">Skip to main content</a>
<div id="container">
	<header role="banner" class="top">
		<div class="content">
			<?php if (is_tree(1378) || is_tax('people_cat', 'iranian-faculty') || is_tax('people_cat', 'iranian-grad') || is_tax('podcast_cat', 'iranian') || has_term('iranian', 'podcast_cat') || is_tax('resources_cat', 'iranian') || has_term('iranian', 'resources_cat')) { ?>
			<a href="/iranian" class="university-logo">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo.svg" alt="UCLA" />
			</a>
			<div class="dept-logo">
				<a href="/iranian" rel="nofollow">
					<img src="<?php echo get_template_directory_uri(); ?>/library/images/iranian-logo.png" alt="Iranian Studies - <?php the_field('department_name', 'option'); ?>" class="dept-logo" />
				</a>
			</div>
			<?php } else { ?>
			<a href="<?php echo home_url(); ?>" class="university-logo">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo.svg" alt="UCLA" />
			</a>
			<div class="dept-logo">
			    <?php // Custom Logo code start
				if (has_custom_logo()) {  // Display the Custom Logo
					the_custom_logo();
				} else {  // No Custom Logo, just display the site's name ?>
				<a href="<?php echo home_url(); ?>"><h1 class="logo-text" alt="<?php the_field('department_name', 'option'); ?>"><?php bloginfo('name'); ?></h1></a>
				<?php } // Custom Logo code ends ?>
			</div>
			<?php } ?>
			<?php if(get_field('enable_donation', 'option') == "enable") { ?>
			<div class="give-back">
				<?php if(get_field('link_type', 'option') == "internal") { ?>
				<a href="<?php the_field('donation_page', 'option'); ?>" class="btn give">
				<?php }?>
				<?php if(get_field('link_type', 'option') == "external") { ?>
				<a href="<?php the_field('donation_link', 'option'); ?>" class="btn give" target="_blank">
				<?php }?><span class="fas fa-heart" aria-hidden="true"></span>
				<?php the_field('button_text', 'option'); ?></a>
				<?php if(get_field('supporting_text', 'option')) { ?>
				<span class="support"><?php the_field('supporting_text', 'option'); ?></span>
				<?php }?>
			</div>
			<?php }?>
		</div>
		<nav role="navigation" aria-labelledby="main navigation" class="desktop">
			<?php wp_nav_menu(array(
				'container' => false,
				'menu' => __( 'Main Menu', 'bonestheme' ),
				'menu_class' => 'main-nav',
				'theme_location' => 'main-nav',
				'before' => '',
				'after' => '',
				'depth' => 2,
			)); ?>
		</nav>
		<?php get_search_form(); ?>
	</header>
	<?php 
		// Don't do any of the below if homepage
		if ( is_front_page() || is_page_template( 'page-home.php' ) ) { }
		// Breadcrumb everywhere else
		elseif ( is_single() || is_category( $category ) || is_archive() ) { 
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			}
		}
		else {
			// Only show hero image on a page or post
			if ( has_post_thumbnail() && is_single() || has_post_thumbnail() && is_page() ) {
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hero' );
				$url = $thumb['0']; ?>
	<div id="hero" style="background-image: url('<?=$url?>');" <?php if(get_field('section_title')) { ?>class="title"<?php } ?>>
		<?php if(get_field('section_title')) { ?>
		<div class="content">
			<span class="image-title"><?php the_field('section_title'); ?></span>
		</div>
		<?php } ?>
		&nbsp;
	</div>
	<?php }
			// And show breadcrumb
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			} 
		} 
	?>