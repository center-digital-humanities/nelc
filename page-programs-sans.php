<?php
/*
 Template Name: Programs Page (no anchor)
*/
?>
<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>
					</article>
					
					<?php if( have_rows('programs') ): ?>
						<ul class="program-list">					
						<?php while( have_rows('programs') ): the_row(); 
							// vars
							$program_title = get_sub_field('program_title');
							$program_description = get_sub_field('description');
							$program_link = get_sub_field('link');
							$additional_link = get_sub_field('secondary_link');
							$additional_link_title = get_sub_field('secondary_link_title');
							?>
							<li class="program">
								<h2>
									<?php if( $program_link ): ?>
									<a href="<?php echo $program_link; ?>">
									<?php endif; ?>
										<?php echo $program_title; ?>
									<?php if( $program_link ): ?>
									</a>
									<?php endif; ?>
								</h2>
							    <?php echo $program_description; ?>
							    <?php if( $program_link ): ?>
							    <a href="<?php echo $program_link; ?>" class="btn">Learn More<span class="hidden"> About <?php echo $program_title; ?></span></a>
							    <?php endif; ?>
							    <?php if( $additional_link ): ?>
							    <a href="<?php echo $additional_link; ?>" class="additional">
							    <?php endif; ?>
							    	<?php echo $additional_link_title; ?>
							    <?php if( $additional_link ): ?>
							    </a>
							    <?php endif; ?>
							</li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
			<?php endwhile; else : ?>
			<?php endif; ?>
<?php get_footer(); ?>