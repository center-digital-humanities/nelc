				<?php if (is_tax('announcement_cat') || is_post_type_archive('announcement') || is_singular ('announcement')){ ?>				
				<div class="col side feed" role="complementary">
					<h3>Announcements</h3>
					<?php
						$posts_query = new WP_Query( array( 'showposts' => 4, 'post_type' => 'announcement') ); ?>
					<ul class="article-list">
						<?php if ($posts_query->have_posts()) : while ($posts_query->have_posts()) : $posts_query->the_post(); ?>
						<li>
								<a href="<?php the_permalink() ?>">
									<h4><?php the_title(); ?></h4>
								</a>
						</li>
						<?php endwhile; ?>
					</ul>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
					<a class="btn" href="/announcement/">View All<span class="hidden"> Announcements</span></a>

					<?php if ( is_active_sidebar( 'events-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'events-sidebar' ); ?>
					<?php else : endif; ?>
				</div>
				<?php } // Event landing page				
				elseif ( tribe_is_past() || tribe_is_upcoming() && !is_tax() ) { 
					// nothing
					
				}  // For posts
				elseif ( is_single() || is_category() || is_search() || is_archive()) { ?>
				<div class="col side feed" role="complementary">
					<?php if ( is_active_sidebar( 'news-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'news-sidebar' ); ?>
					<?php else : endif; ?>
					<?php if ( is_active_sidebar( 'events-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'events-sidebar' ); ?>
					<?php else : endif; ?>
				</div>
				<?php } ?>
				<?php // For pages
				if (is_page() || is_404()) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Graduate subpage
								if (is_tree(2155) || get_field('menu_select') == "graduate") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Graduate', 'bonestheme' ),
										'menu_class' => 'graduate-nav',
										'theme_location' => 'graduate-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Graduate</h3> <ul>%3$s</ul>'
									));
								}
								
								// If an Undergraduate subpage
								if (is_tree(860) || get_field('menu_select') == "undergraduate") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Undergraduate', 'bonestheme' ),
									   	'menu_class' => 'undergrad-nav',
									   	'theme_location' => 'undergrad-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Undergraduate</h3> <ul>%3$s</ul>'
									));
								}
								
								// If Ancient Near Eastern Civilizations subpage
								if (is_tree(1447) || get_field('menu_select') == "ancient") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Ancient Civilizations', 'bonestheme' ),
									   	'menu_class' => 'ancient-nav',
									   	'theme_location' => 'ancient-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Ancient Civilizations</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'ancient', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/ancient/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Ancient Near East & Egyptology subpage
								if (is_tree(1475) || get_field('menu_select') == "ancient-egyptology") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Ancient Near East & Egyptology', 'bonestheme' ),
									   	'menu_class' => 'ancient-egyptology-nav',
									   	'theme_location' => 'ancient-egyptology-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Ancient Near East & Egyptology</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'ancient-egyptology', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/ancient-egyptology/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Arabic subpage
								if (is_tree(1449) || get_field('menu_select') == "arabic") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Arabic', 'bonestheme' ),
									   	'menu_class' => 'arabic-nav',
									   	'theme_location' => 'arabic-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Arabic</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'arabic', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/arabic/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Arabic & Islamic Studies subpage
								if (is_tree(3033) || get_field('menu_select') == "arabic-islamic") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Arabic & Islamic', 'bonestheme' ),
									   	'menu_class' => 'arabic-islamic-nav',
									   	'theme_location' => 'arabic-islamic-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Arabic & Islamic</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'arabic-islamic', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/arabic-islamic/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Armenian subpage
								if (is_tree(1370) || get_field('menu_select') == "armenian") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Armenian Studies', 'bonestheme' ),
									   	'menu_class' => 'armenian-nav',
									   	'theme_location' => 'armenian-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Armenian Studies</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'armenian', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/armenian/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Assyriology subpage
								if (is_tree(1451) || get_field('menu_select') == "assyriology") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Assyriology', 'bonestheme' ),
									   	'menu_class' => 'assyriology-nav',
									   	'theme_location' => 'assyriology-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Assyriology</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'assyriology', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/assyriology/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Egyptology subpage
								if (is_tree(1454) || get_field('menu_select') == "egyptology") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Egyptology', 'bonestheme' ),
									   	'menu_class' => 'egyptology-nav',
									   	'theme_location' => 'egyptology-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Egyptology</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'egyptology', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/egyptology/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Hebrew Bible subpage
								if (is_tree(1456) || get_field('menu_select') == "hebrew") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Hebrew Bible', 'bonestheme' ),
									   	'menu_class' => 'hebrew-nav',
									   	'theme_location' => 'hebrew-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Hebrew Bible</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'hebrew', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/hebrew/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Hebrew & Jewish Studies subpage
								if (is_tree(1479) || get_field('menu_select') == "hebrew-jewish") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Hebrew & Jewish', 'bonestheme' ),
									   	'menu_class' => 'hebrew-jewish-nav',
									   	'theme_location' => 'hebrew-jewish-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Hebrew & Jewish</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'hebrew-jewish', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/hebrew-jewish/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Iranian Studies subpage
								if (is_tree(1378) || get_field('menu_select') == "iranian") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Iranian Studies', 'bonestheme' ),
									   	'menu_class' => 'iranian-nav',
									   	'theme_location' => 'iranian-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Iranian Studies</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'iranian', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol class="event-list">
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/iranian/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Islamic Studies subpage
								if (is_tree(1458) || get_field('menu_select') == "islamic") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Islamic Studies', 'bonestheme' ),
									   	'menu_class' => 'islamic-nav',
									   	'theme_location' => 'islamic-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Islamic Studies</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'islamic', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/islamic/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Israel Studies subpage
								if (is_tree(1481) || get_field('menu_select') == "israel") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Israel Studies', 'bonestheme' ),
									   	'menu_class' => 'israel-nav',
									   	'theme_location' => 'israel-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Israel Studies</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'israel', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/israel/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Jewish Studies subpage
								if (is_tree(1483) || get_field('menu_select') == "jewish") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Jewish Studies', 'bonestheme' ),
									   	'menu_class' => 'jewish-nav',
									   	'theme_location' => 'jewish-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Jewish Studies</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'jewish', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/jewish/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Levantine Studies subpage
								if (is_tree(1460) || get_field('menu_select') == "levantine") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Levantine Studies', 'bonestheme' ),
									   	'menu_class' => 'levantine-nav',
									   	'theme_location' => 'levantine-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Levantine Studies</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'levantine', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/levantine/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Middle Eastern Studies subpage
								if (is_tree(1485) || get_field('menu_select') == "middle-eastern") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Middle Eastern Studies', 'bonestheme' ),
									   	'menu_class' => 'middle-eastern-nav',
									   	'theme_location' => 'middle-eastern-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Middle Eastern Studies</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'middle-eastern', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/middle-eastern/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Near Eastern Archaeology subpage
								if (is_tree(1462) || get_field('menu_select') == "archaeology") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Near Eastern Archaeology', 'bonestheme' ),
									   	'menu_class' => 'archaeology-nav',
									   	'theme_location' => 'archaeology-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Archaeology</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'archaeology', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/archaeology/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Semitics subpage
								if (is_tree(1464) || get_field('menu_select') == "semitics") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Semitics', 'bonestheme' ),
									   	'menu_class' => 'semitics-nav',
									   	'theme_location' => 'semitics-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Semitics</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'semitics', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/semitics/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}
								
								// If Turkic Studies subpage
								if (is_tree(1466) || get_field('menu_select') == "turkic") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Turkic Studies', 'bonestheme' ),
									   	'menu_class' => 'turkic-nav',
									   	'theme_location' => 'turkic-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Turkic Studies</h3> <ul>%3$s</ul>'
									));
								?>
								<?php $events = new WP_Query( array( 'tribe_events_cat' => 'turkic', 'post_type' => 'tribe_events', 'posts_per_page' => 5 ) );
								if ( $events->have_posts() ) { ?>
									<ol>
										<h3>Events</h3>
									<?php while ( $events->have_posts() ) : $events->the_post(); ?>
										<li>
											<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
											<div class="duration">
												<?php echo tribe_events_event_schedule_details(); ?>
											</div>
										</li>
									<?php endwhile; ?>
									</ol>
									<a class="btn" href="/events/category/turkic/">View All <span class="hidden"> Events</span></a>
								<?php } 
								}

								// For Search, 404's, or other pages you want to use it on
								// Replace 9999 with id of parent page
								if (is_tree(1439) || is_tree(1437) || is_search() || is_404() || is_page('contact') || is_page('about') || is_page('give') || get_field('menu_select') == "general") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Main Menu', 'bonestheme' ),
										'menu_class' => 'side-nav',
										'theme_location' => 'main-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
									));
								}
							?>
						</nav>
						<?php if ( is_page( 'give' )  ) : ?>
							<?php dynamic_sidebar( 'give-sidebar' ); ?>
						<?php else : ?>
						<?php endif; ?>
					</div>
				</div>
				<?php } ?>