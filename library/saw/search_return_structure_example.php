<?php
//Sample Array Return
[LING] => [
    [0001] => [
        'subjectClass' => "1 - Introduction to Study of Language"
        'careerLevel' => 'U',
        'subjectAreaCode' => "CHIN", // the subject area as the term code
        'courseCatalogNumber' => "0001", // the real course catalog number
        'courseCatalogNumberDisplay' => "1", 
        'courseShortTitle' => "INTR-STUDY-LANGUAGE",
        'courseLongTitle' => "Introduction to Study of Language",
        'courseDescription' => "Lecture, three hours; discussion, one hour. Summary, for general..."
        'classNumber' => "001" 
        'classTitle' => "Introduction to Study of Language",
        'classDescription' => "",
            ['instructors'] => [
                [0] =>[ 
                    'firstName' => 'Lucian', // first name of instructor
                    'middleName' => 'Danger', // middle name of instructor
                    'lastName' => 'Tucker', // last name of instructor
                    'fullName' => 'Lucian "Danger" Tucker', // instructor's full name
                ]
                [2] =>[ 
                    'firstName' => 'Lucian', // first name of instructor
                    'middleName' => 'Danger', // middle name of instructor
                    'lastName' => 'Tucker', // last name of instructor
                    'fullName' => 'Lucian "Danger" Tucker', // instructor's full name
                ]
            ]
        ]
    ]
]