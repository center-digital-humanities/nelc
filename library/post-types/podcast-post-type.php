<?php
// Podcast Post Type Settings

// add custom categories
register_taxonomy( 'podcast_cat', 
	array('podcast'), /* if you change the name of register_post_type( 'Podcast', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Podcast Categories', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Podcast Category', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Podcast Categories', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Podcast Categories', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Podcast Category', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Podcast Category:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Podcast Category', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Podcast Category', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Podcast Category', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Podcast Category Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'podcasts' )
	)
);

// let's create the function for the custom type
function Podcast_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'podcast', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Podcasts', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Podcast', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Podcast', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Podcast', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Podcast', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Podcast', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Podcast', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Podcast', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No Podcast added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'All podcast for department', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 6, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-playlist-audio', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'podcast', 'with_front' => true ), /* you can specify its url slug */
			'capability_type' => 'post',
			'hierarchical' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'revisions', 'author' )
			
		) /* end of options */
	); /* end of register post type */
}

// adding the function to the Wordpress init
add_action( 'init', 'podcast_post_type');	
?>