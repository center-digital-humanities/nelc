<?php
// People Post Type Settings

// let's create the function for the custom type
function conference_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'conference', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Conferences', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Conference', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Conferences', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Conference', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Conference', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Conference', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Conference', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Conferences', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No conference added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Contains all department conference', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 5, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-groups', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'conference', 'with_front' => true ), /* you can specify its url slug */
			'capability_type' => 'post',
			'hierarchical' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'revisions', 'author', 'page-attributes', 'thumbnail' )
			
		) /* end of options */
	); /* end of register post type */
}

// adding the function to the Wordpress init
add_action( 'init', 'conference_post_type');	
?>