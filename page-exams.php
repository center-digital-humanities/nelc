<?php
/*
 Template Name: Exams Page
*/
?>
<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>
					</article>
					<?php // Programs ?>
					
					<?php if( have_rows('programs') ): ?>
						<ul>
						<?php while( have_rows('programs') ): the_row(); 
							// vars
							$program_title = get_sub_field('program_title');
							$item_string = str_replace(array(".", ",","-", "(", ")", "'"," "), '' , $program_title);
							?>
							<li>
								<a href="#<?php echo $item_string; ?>"><?php echo $program_title; ?></a>
							</li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>
					<?php if( have_rows('programs') ): ?>
						<ul class="program-list">					
						<?php while( have_rows('programs') ): the_row(); 
							// vars
							$program_title = get_sub_field('program_title');
							$item_string = str_replace(array(".", ",","-", "(", ")", "'"," "), '' , $program_title);
							$program_description = get_sub_field('description');
							$program_link = get_sub_field('link');
							$additional_link = get_sub_field('secondary_link');
							$additional_link_title = get_sub_field('secondary_link_title');
							?>
							<li id="<?php echo $item_string; ?>" class="program">
								<h2>
									<?php if( $program_link ): ?>
									<a href="<?php echo $program_link; ?>">
									<?php endif; ?>
										<?php echo $program_title; ?>
									<?php if( $program_link ): ?>
									</a>
									<?php endif; ?>
								</h2>
							    <?php echo $program_description; ?>
							    <?php if( $program_link ): ?>
							    <a href="<?php echo $program_link; ?>" class="btn">Register<span class="hidden"> For <?php echo $program_title; ?> Exam</span></a>
							    <?php endif; ?>
							    <?php if( $secondary_link ): ?>
							    <a href="<?php echo $secondary_link; ?>" class="additional">
							    <?php endif; ?>
							    	<?php echo $secondary_link_title; ?>
							    <?php if( $secondary_link ): ?>
							    </a>
							    <?php endif; ?>
							</li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
			<?php endwhile; else : ?>
			<?php endif; ?>
<?php get_footer(); ?>