<?php
/*
 Template Name: Resources Page
*/
?>
<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>
					</article>
					<?php // Resources ?>
					
					<?php if( have_rows('resources') ): ?>
						<ul>
						<?php while( have_rows('resources') ): the_row(); 
							// vars
							$program_title = get_sub_field('program_title');
							$item_string = str_replace(array(".", ",","-", "(", ")", "'"," "), '' , $program_title);
							?>
							<li>
								<a href="#<?php echo $item_string; ?>"><?php echo $program_title; ?></a>
							</li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>
					<?php if( have_rows('resources') ): ?>
						<ul class="program-list">					
						<?php while( have_rows('resources') ): the_row(); 
							// vars
							$program_title = get_sub_field('program_title');
							$program_description = get_sub_field('description');
							$program_link = get_sub_field('link');
							$additional_link = get_sub_field('secondary_link');
							$additional_link_title = get_sub_field('secondary_link_title');
							
							$program_email = get_sub_field('email');
							$program_phone = get_sub_field('phone_number');
							$program_type = get_sub_field('resource_type');
							?>
							<li id="<?php echo $item_string; ?>" class="program">
								<h2>
									<?php if( $program_link ): ?>
									<a href="<?php echo $program_link; ?>">
									<?php endif; ?>
										<?php echo $program_title; ?>
									<?php if( $program_link ): ?>
									</a>
									<?php endif; ?>
								</h2>
								<?php if( $program_type ): ?>
								<span class="program-type"><?php echo $program_type; ?></span>
								<?php endif; ?>
								<?php if( $program_email ): ?>
								<span class="program-email"><strong>Email:</strong> <a href="mailto:<?php echo $program_email; ?>"><?php echo $program_email; ?></a></span>
								<?php endif; ?>
								<?php if( $program_phone ): ?>
								<span class="program-phone"><strong>Phone:</strong> <?php echo $program_phone; ?></span>
								<?php endif; ?>
							    <?php echo $program_description; ?>
							    <?php if( $program_link ): ?>
							    <a href="<?php echo $program_link; ?>" class="btn">Visit Website <span class="hidden"> for <?php echo $program_title; ?></span></a>
							    <?php endif; ?>
							    <?php if( $additional_link ): ?>
							    <a href="<?php echo $additional_link; ?>" class="additional">
							    <?php endif; ?>
							    	<?php echo $additional_link_title; ?>
							    <?php if( $additional_link ): ?>
							    </a>
							    <?php endif; ?>
							</li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
			<?php endwhile; else : ?>
			<?php endif; ?>
<?php get_footer(); ?>