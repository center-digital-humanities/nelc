<?php
/*
 Template Name: Give Back Page
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
							<?php if( have_rows('give_back_images') ): ?>
							
								<ul class="giving">
							
								<?php while( have_rows('give_back_images') ): the_row(); 
									// vars
									$giving_image = get_sub_field('give_image');
									$content = get_sub_field('give_text');
									
									if( !empty($giving_image) ): 
										// thumbnail
										$size = 'giving-photo';
										$photo = $giving_image['sizes'][ $size ];
										$width = $giving_image['sizes'][ $size . '-width' ];
										$height = $giving_image['sizes'][ $size . '-height' ];
									endif;
								?>
									<li style="background: url('<?php echo $photo; ?>') no-repeat left top; background-size: cover;">
										<div class="overlay">
											<p><?php echo $content; ?></p>
										</div>
									</li>
							
								<?php endwhile; ?>
							
								</ul>
							
							<?php endif; ?>
						</section>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>